# Teoria 1
## ukrywanie elementów
Możesz całkowicie wyłączyć wyświetlanie jakiegoś elementu nadając mu styl `display: none`.<br>
Zobacz przykład poniżej
```css
div {
	display: none;
}
```
