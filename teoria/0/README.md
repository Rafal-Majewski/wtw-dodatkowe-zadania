# Teoria 0
## border-box
Dla ułatwienia tworzenia stron internetowych musisz korzystać z następującego stylu.
```css
* {
	box-sizing: border-box;
}
```
Zapoznaj się z przykładem w `box sizing/box-sizing.html`.<br>
Albo przeczytaj o box-sizing na [w3schools](https://www.w3schools.com/css/css3_box-sizing.asp)

## przykładowa strona
Możesz zapoznać się z przykładowym ostylowanym układem strony w folderze `przykładowa strona`.<br>
W pliku `styles.scss` znajdują się wyjaśnienia użytych stylów.<br>

