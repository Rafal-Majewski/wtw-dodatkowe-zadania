# Zadanie 1
Używając podanego `index.html` ostyluj w SCSS stronę tak, aby wyglądała podobnie do tej z obrazków poniżej. Plik nazwij `styles.scss`.

Kolory trzymaj w zmiennych.<br>
Zauważ, że na małym ekranie niektóre elementy mają się pojawić, a inne zniknąć. W media queries skorzystaj z klas `displayBig` i `displaySmall`.<br>
Ostyluj zdarzenia najechania i kliknięcia na przyciski.

Możesz skorzystać z kolorów użytych w przykładzie:
```css
$dark1: hsl(0, 0, 10);
$dark2: hsl(0, 0, 20);
$dark3: hsl(0, 0, 30);
$dark4: hsl(0, 0, 40);
$primary0: hsl(210, 75, 50);
$primary1: hsl(210, 75, 40);
$primary2: hsl(210, 75, 30);
$primaryText: white;
$secondary0: hsl(30, 75, 50);
$secondary1: hsl(30, 75, 40);
$secondary2: hsl(30, 75, 30);
$secondaryText: black;
```

## Strona na dużym ekranie (od 701 px szerokości)
![pc](./pc.png)

## Strona na małym ekranie (do 700 px szerokości)
![mobile](./mobile.png)

## Efekty
![efekty](./effects.gif)

## Zmiana rozmiaru okna
![resizing](./resizing.gif)


