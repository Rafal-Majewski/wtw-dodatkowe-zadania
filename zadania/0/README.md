# Zadanie 0
Używając podanego `index.html` ostyluj w SCSS stronę tak, aby wyglądała podobnie do tej z obrazków poniżej. Plik nazwij `styles.scss`.

## Strona na dużym ekranie (od 601 px szerokości)
![pc](./pc.png)

## Strona na małym ekranie (do 600 px szerokości)
![mobile](./mobile.png)
