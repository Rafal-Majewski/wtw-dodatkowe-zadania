# Zadanie 2
Używając podanego `index.html` ostyluj w SCSS stronę tak, aby wyglądała podobnie do tej z obrazków poniżej. Plik nazwij `styles.scss`.

Kolory trzymaj w zmiennych.<br>
Plan zajęć musi być gridem. Przy stylowaniu jego elementów skorzystaj z pętli SCSS!<br>
Ostyluj zdarzenia najechania i kliknięcia na przyciski.<br>
W tym zadaniu nie jest wymagana responsywność.

Możesz skorzystać z kolorów użytych w przykładzie:
```css
$dark55: hsl(0, 0, 55);
$dark75: hsl(0, 0, 75);
$dark85: hsl(0, 0, 85);
$primary5: hsl(210, 100, 5);
$primary15: hsl(210, 100, 15);
$primary25: hsl(210, 100, 25);
$primary45: hsl(210, 100, 45);
$primary65: hsl(210, 100, 65);
$primary75: hsl(210, 100, 75);
$primary85: hsl(210, 100, 85);
$primary95: hsl(210, 100, 95);
$secondary5: hsl(30, 100, 5);
$secondary15: hsl(30, 100, 15);
$secondary25: hsl(30, 100, 25);
$secondary35: hsl(30, 100, 35);
$secondary45: hsl(30, 100, 45);
$secondary55: hsl(30, 100, 55);
```

## Strona
![website](./website.png)

## Efekty
![efekty](./effects.gif)



