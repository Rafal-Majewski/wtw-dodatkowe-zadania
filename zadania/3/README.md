# Zadanie 3
Używając podanego `index.html` ostyluj w SCSS stronę tak, aby wyglądała podobnie do tej z obrazków poniżej. Plik nazwij `styles.scss`.

Przy stylowaniu planszy w Paket Trakerze użyj pozycjonowania relatywnego.<br>
Ostyluj zdarzenia najechania na przyciski.<br>
W tym zadaniu nie jest wymagana responsywność.

Możesz skorzystać z kolorów użytych w przykładzie:
```css
$primary: #00BCEB;
$secondary: #eb2f00;
```

## Strona
![website](./website.png)

## Efekty w navbarze
![efekty](./effects_nav.gif)

## Efekty w głównej części strony
![efekty](./effects_main.gif)



