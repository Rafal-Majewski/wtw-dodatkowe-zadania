# wtw-dodatkowe-zadania
Repozytorium z dodatkowymi zadaniami na stylowanie stron używając SCSS.

Swoje rozwiązania trzymaj w folderze `rozwiazania` (utwórz go).<br>
Jeśli chcesz pobawić się innymi plikami, utwórz folder `piaskownica` i umieść tam wybrane pliki.<br>
Nie edytuj ani nie twórz plików poza folderami `rozwiazania` i `piaskownica`.<br>
Żeby wyświetlić plik README.md w Visual Studio Code w czytelnej postaci otwórz wybrany plik README.md i kliknij `CTRL+SHIFT+V`.